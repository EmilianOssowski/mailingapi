﻿using MailingAPI.Abstract;
using System.Collections.Generic;

namespace MailingAPI.Models
{
    public class Group : BaseModel
    {
        public Group() : base()
        {
            Recipients = new List<Recipient>();
        }
        public string Label { get; set; }
        public virtual List<Recipient> Recipients { get; set; }
        public int UserId { get; internal set; }
    }
}
