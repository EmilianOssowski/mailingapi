﻿using MailingAPI.Abstract;
using Newtonsoft.Json;

namespace MailingAPI.Models
{
    public class Recipient : BaseModel
    {
        public Recipient() : base() { }

        public string Mail { get; set; }
        public int GroupId { get; set; }
        [JsonIgnore]
        public Group Group { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}
