﻿using MailingAPI.Abstract;
using MailingAPI.DataTransferObjects.User;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MailingAPI.Models
{
    public class User : BaseModel
    {
        public User() : base()
        {
            Groups = new List<Group>();
        }

        public User(UserRegisterDTO model) : base()
        {
            Groups = new List<Group>();
            Firstname = model.Firstname;
            Lastname = model.Lastname;
            Username = model.Username;
        }

        [Required]
        public string Username { get; set; }
        public byte[] PasswordSalt { get; internal set; }
        public byte[] PasswordHash { get; internal set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public virtual List<Group> Groups { get; set; }


    }
}
