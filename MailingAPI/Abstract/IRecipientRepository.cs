﻿using MailingAPI.Models;
using System.Linq;
using System.Threading.Tasks;

namespace MailingAPI.Abstract
{
    public interface IRecipientRepository
    {
        IQueryable<Recipient> GetAll();
        IQueryable<Recipient> GetRecipientsByGroupId(int groupId);
        Recipient GetById(int id);
        Task<Recipient> AddAsync(Recipient entity);
        Task<Recipient> UpdateRecipientAsync(int id, Recipient model);
        Task<Recipient> DeleteAsync(Recipient entity);
        bool Exists(int id);
 

    }
}
