﻿using System;

namespace MailingAPI.Abstract
{
    public abstract class BaseModel
    {
        public BaseModel()
        {
            CreatedAt = DateTime.Now;
        }
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
