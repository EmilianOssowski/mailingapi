﻿using MailingAPI.Models;
using System.Linq;
using System.Threading.Tasks;

namespace MailingAPI.Abstract
{
    public interface IGroupRepository
    {
        IQueryable<Group> GetAll();
        Task<Group> GetGroupByIdAndUserId(int id, int userId);
        IQueryable<Group> GetGroupsByUserId(int userId);
        Task<Group> AddAsync(Group entity);
        Task<Group> UpdateGroupAsync(int id, Group model, int userId);
        Task<Group> DeleteAsync(Group entity);
        bool Exists(int id);
        bool IsUserAuthorized(int groupid, int userId);
    }
}
