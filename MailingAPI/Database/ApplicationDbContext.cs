﻿using MailingAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace MailingAPI.Database
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Recipient> Recipients { get; set; }
    }
}
