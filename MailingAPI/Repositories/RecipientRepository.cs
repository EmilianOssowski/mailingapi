﻿using MailingAPI.Abstract;
using MailingAPI.Database;
using MailingAPI.Models;
using System.Linq;
using System.Threading.Tasks;

namespace MailingAPI.Repositories
{
    public class RecipientRepository : Repository<Recipient>, IRecipientRepository
    {
        public RecipientRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Recipient GetById(int id)
        {
            return _context.Recipients.Where(x => x.Id == id).FirstOrDefault();
        }

        public IQueryable<Recipient> GetRecipientsByGroupId(int groupId)
        {
            return GetAll().Where(s => s.GroupId == groupId);
        }

        public async Task<Recipient> UpdateRecipientAsync(int id, Recipient model)
        {
            var entity = GetById(id);
            if (entity != null)
            {
                entity.GroupId = model.GroupId;
                entity.Mail = entity.Mail;
                entity.Firstname = entity.Firstname;
                entity.Lastname = entity.Lastname;
                await UpdateAsync(entity);
                return entity;
            }
            return null;
        }
    }
}
