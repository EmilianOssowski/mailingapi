﻿using MailingAPI.Abstract;
using MailingAPI.Database;
using MailingAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace MailingAPI.Repositories
{
    public class GroupRepository : Repository<Group>, IGroupRepository
    {
        public GroupRepository(ApplicationDbContext context) : base(context)
        {
        }


        public Task<Group> GetGroupByIdAndUserId(int id, int userId)
        {
            return GetAll().Where(s => s.Id == id && s.UserId == userId).FirstOrDefaultAsync();
        }

        public IQueryable<Group> GetGroupsByUserId(int userId)
        {
            return GetAll().Where(s => s.UserId == userId).AsQueryable();
        }

        public async Task<Group> UpdateGroupAsync(int id, Group model, int userId)
        {
            var entity = GetAll().Where(s => s.Id == id && s.UserId == userId).FirstOrDefault();
            if (entity != null)
            {
                entity.Label = model.Label;
                await UpdateAsync(entity);
                return entity;
            }
            return null;
        }
        public bool IsUserAuthorized(int groupid, int userId)
        {
            var entity = GetAll().Where(s => s.Id == groupid).FirstOrDefault();
            if (entity != null && entity.UserId == userId)
            {
                return true;
            }
            return false;
        }

    }
}
