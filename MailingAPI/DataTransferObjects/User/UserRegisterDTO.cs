﻿namespace MailingAPI.DataTransferObjects.User
{
    public class UserRegisterDTO
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
