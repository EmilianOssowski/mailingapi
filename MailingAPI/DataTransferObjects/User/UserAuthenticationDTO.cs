﻿namespace MailingAPI.DataTransferObjects.User
{
    public class UserAuthenticationDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
