﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MailingAPI.Migrations
{
    public partial class AddFirstnameAndLastnameToRecipient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Firstname",
                table: "Recipients",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lastname",
                table: "Recipients",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Firstname",
                table: "Recipients");

            migrationBuilder.DropColumn(
                name: "Lastname",
                table: "Recipients");
        }
    }
}
