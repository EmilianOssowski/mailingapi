﻿using MailingAPI.Abstract;
using MailingAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MailingAPI.Controllers
{
    [Route("api/group/{groupId}/[controller]")]
    [ApiController]
    public class RecipientController : ControllerBase
    {
        private readonly IRecipientRepository _recipientRepository;
        private readonly IGroupRepository _groupRepository;

        public RecipientController(IRecipientRepository recipientRepository, IGroupRepository groupRepository)
        {
            _recipientRepository = recipientRepository;
            _groupRepository = groupRepository;
        }

        /// <summary>
        /// Gets all available recipients data for user
        /// </summary>
        /// <param name="groupId">Id of group which should be selected recipients</param>
        /// <returns>List of objects of type Recipient</returns>
        /// <response code="200">List of recipients, if group does not have recipients returns empty list</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        /// <response code="404">Group with provided Id not found</response>    
        /// <response code="401">User unautorized</response> 
        // GET: api/Recipient
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Recipient>>> GetRecipients([FromRoute]int groupId)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            if (!_groupRepository.Exists(groupId))
                return NotFound();

            if (!IsUserAuthorized(groupId, userId))
                return Forbid();

            return await _recipientRepository.GetRecipientsByGroupId(groupId).ToListAsync();
        }
        /// <summary>
        /// Gets available recipient data
        /// </summary>
        /// <param name="groupId">Id of group which should be selected recipients</param>
        /// <param name="id">Id of recipient</param>
        /// <returns>List of objects of type Recipient</returns>
        /// <response code="200">Recipient data</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        /// <response code="404">Group with provided Id not found</response>    
        /// <response code="401">User unautorized</response> 
        // GET: api/Recipient/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Recipient>> GetRecipient([FromRoute]int groupId, int id)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            var recipient = _recipientRepository.GetById(id);

            if (recipient == null)
            {
                return NotFound();
            }
            if (!IsUserAuthorized(groupId, userId))
            {
                return Forbid();
            }

            return recipient;
        }
        /// <summary>
        /// Updates recipient
        /// </summary>
        /// <param name="groupId">Id of group</param>
        /// <param name="id">Id of recipient</param>
        /// <returns>Updated recipient data</returns>
        /// <response code="200">List of groups, if user don't have groups returns empty list</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        /// <response code="403">New group with provided Id does not belong to this user</response>    
        /// <response code="404">Group with provided Id not found</response> 
        /// <response code="406">Query id does not cover with body id</response> 
        /// <response code="401">User unautorized</response> 
        // PUT: api/Recipient/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<ActionResult<Recipient>> PutRecipient([FromRoute]int groupId, int id, [FromBody]Recipient recipient)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            if (!RecipientExists(id))
                return NotFound();

            if (!IsUserAuthorized(groupId, userId))
                return Forbid("Group with provided Id does not belong to this user");
            if(groupId != recipient.GroupId && !IsUserAuthorized(recipient.GroupId, userId))
                return Forbid("New group with provided Id does not belong to this user");


            if (id != recipient.Id)
                return BadRequest();

            try
            {
                return await _recipientRepository.UpdateRecipientAsync(id, recipient);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecipientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }
        /// <summary>
        /// Creates recipient data
        /// </summary>
        /// <param name="groupId">Id of group</param>
        /// <param name="recipient">Recipient data</param>
        /// <returns>Created recipient data</returns>
        /// <response code="200">List of groups, if user don't have groups returns empty list</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        /// <response code="403">New group with provided Id does not belong to this user</response>    
        /// <response code="404">Group with provided Id not found</response> 
        /// <response code="406">Query id does not cover with body id</response> 
        /// <response code="401">User unautorized</response> 
        // POST: api/Recipient
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Recipient>> PostRecipient([FromRoute]int groupId, [FromBody]Recipient recipient)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            if (!_groupRepository.Exists(groupId))
                return NotFound();

            if (!IsUserAuthorized(groupId, userId))
                return Forbid();

            if (groupId != recipient.GroupId)
                return BadRequest();

            recipient.GroupId = groupId;
            await _recipientRepository.AddAsync(recipient);

            return CreatedAtAction("GetRecipient", new { id = recipient.Id, groupId= recipient.GroupId }, recipient);
        }
        /// <summary>
        /// Deletes recipient
        /// </summary>
        /// <param name="id">Id of recipient</param>
        /// <param name="groupId">Id of group</param>
        /// <returns>Deleted recipient data</returns>
        /// <response code="200">Recipient deleted</response>     
        /// <response code="404">Recipient with provided Id not found</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        /// <response code="401">User unautorized</response> 
        // DELETE: api/Recipient/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Recipient>> DeleteRecipient([FromRoute]int groupId, int id)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            var group = await _groupRepository.GetGroupByIdAndUserId(id, userId);
            var recipient = _recipientRepository.GetById(id);
            if (group == null || recipient == null)
                return NotFound();

            if (!IsUserAuthorized(groupId, userId))
                return Forbid();

            await _recipientRepository.DeleteAsync(recipient);

            return recipient;
        }

        private bool RecipientExists(int id)
        {
            return _recipientRepository.Exists(id);
        }
        private bool IsUserAuthorized(int id, int userId)
        {
            return _groupRepository.IsUserAuthorized(id, userId);
        }
    }
}
