﻿using MailingAPI.DataTransferObjects.User;
using MailingAPI.Models;
using MailingAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MailingAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Authenticate user by username and password in body
        /// </summary>
        /// <param name="model"></param>
        /// <returns>User data with JWT token, which should be used for Authorization (Bearer)</returns>
        /// <response code="200">Returns the user data with JWT token</response>
        /// <response code="401">If the username or password is wrong</response>    
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]UserAuthenticationDTO model)
        {
            var user = _userService.Authenticate(model.Username, model.Password);

            if (user == null)
                return Unauthorized(new { message = "Username or password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("very very secret key");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info and authentication token
            return Ok(new
            {
                Id = user.Id,
                Username = user.Username,
                FirstName = user.Firstname,
                LastName = user.Lastname,
                Token = tokenString
            });
        }
        /// <summary>
        /// Creates user, which allows to use API
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Status of operation</returns>
        /// <response code="200">User was created</response>
        /// <response code="409">User with this username already exists</response>
        /// <response code="406">Error while creating user</response>
        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]UserRegisterDTO model)
        {

            if (_userService.GetByUsername(model.Username) != null)
            {
                return Conflict("User with this username already exists");
            }
            // map model to entity
            var user = new User(model);

            try
            {
                // create user
               _userService.Create(user, model.Password);
                return Ok();
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}
