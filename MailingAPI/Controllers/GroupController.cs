﻿using MailingAPI.Abstract;
using MailingAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MailingAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IGroupRepository _repository;

        public GroupController( IGroupRepository groupRepository)
        {
            _repository = groupRepository;
        }
        /// <summary>
        /// Gets all available groups data for user
        /// </summary>
        /// <param name="includeRecipients">Decides if group will have nested recipients (default: false)</param>
        /// <returns>List of objects of type Group</returns>
        /// <response code="200">List of groups, if user don't have groups returns empty list</response>    
        /// <response code="401">User unautorized</response> 
        // GET: api/Group
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Group>>> GetGroups(bool includeRecipients = false)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            var query = _repository.GetGroupsByUserId(userId);
            if (includeRecipients)
                query = query.Include(s => s.Recipients);
            
            return await query.ToListAsync();
        }
        /// <summary>
        /// Gets available group data
        /// </summary>
        /// <param name="id">Id of group</param>
        /// <returns>List of objects of type Group</returns>
        /// <response code="200">Data of group with provided id</response>    
        /// <response code="404">Group with provided Id not found</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        /// <response code="401">User unautorized</response> 
        // GET: api/Group/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Group>> GetGroup(int id)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            var @group = await _repository.GetGroupByIdAndUserId(id, userId);

            if (@group == null)
            {
                return NotFound();
            }
            if (!IsUserAuthorized(id, userId))
            {
                return Forbid();
            }

            return @group;
        }
        /// <summary>
        /// Updates group
        /// </summary>
        /// <param name="id">Id of group</param>
        /// <param name="group">Group model to change</param>
        /// <returns>Updated group data</returns>
        /// <response code="200">Group updated</response>    
        /// <response code="404">Group with provided Id not found</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        /// <response code="409">Group with provided label already exists</response>    
        /// <response code="406">Query id does not cover with body id</response>    
        /// <response code="401">User unautorized</response> 
        // PUT: api/Group/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<ActionResult<Group>> PutGroup(int id, [FromBody]Group group)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            if (!GroupExists(id))
                return NotFound();
            
            if (!IsUserAuthorized(id, userId))
                return Unauthorized();
            if (ValidateEntity(group))
                return Conflict("Group with this label already exists");

            if (id != group.Id)
            {
                return BadRequest();
            }

            try
            {
                return await _repository.UpdateGroupAsync(id, group, userId);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

        }
        /// <summary>
        /// Adds group
        /// </summary>
        /// <param name="group">Group model</param>
        /// <returns>Created group data</returns>
        /// <response code="200">Group created</response>  
        /// <response code="401">User unautorized</response> 
        /// <response code="409">Group with provided label already exists</response>    
        // POST: api/Group
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Group>> PostGroup([FromBody]Group group)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            if (ValidateEntity(group))
                return Conflict("Group with this label already exists");
            group.UserId = userId;
            await _repository.AddAsync(group);

            return CreatedAtAction("GetGroup", new { id = group.Id }, group);
        }
        /// <summary>
        /// Deletes group
        /// </summary>
        /// <param name="id">Id of group</param>
        /// <returns>Deleted group data</returns>
        /// <response code="200">Group deleted</response>     
        /// <response code="401">User unautorized</response> 
        /// <response code="404">Group with provided Id not found</response>    
        /// <response code="403">Group with provided Id does not belong to this user</response>    
        // DELETE: api/Group/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Group>> DeleteGroup(int id)
        {
            var userId = int.Parse(this.User.Claims.First(i => i.Type == ClaimTypes.Name).Value);
            var group = await _repository.GetGroupByIdAndUserId(id, userId);
            if (group == null)
            {
                return NotFound();
            }
            if (!IsUserAuthorized(id, userId))
            {
                return Forbid();
            }

            await _repository.DeleteAsync(group);

            return group;
        }

        private bool GroupExists(int id)
        {
            return _repository.Exists(id);
        }

        private bool IsUserAuthorized(int id, int userId)
        {
            return _repository.IsUserAuthorized(id, userId);
        }

        private bool ValidateEntity(Group entity)
        {
            return !_repository.GetAll().Where(s => s.Label == entity.Label && s.Id != entity.Id).Any();
        }
    }
}
